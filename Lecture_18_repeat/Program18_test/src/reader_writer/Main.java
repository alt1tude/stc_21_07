package reader_writer;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.CharBuffer;

public class Main {
    public static void main(String[] args) throws Exception {
        Reader reader = new FileReader("input.txt");
//        int fromFile = reader.read();
        char array[] = new char[10];
        CharBuffer buffer = CharBuffer.wrap(array);
        reader.read(buffer);
        System.out.println(new String(buffer.array()));
//        System.out.println((char) fromFile);

        Writer write = new FileWriter("output.txt", true);
        write.write("Qwerty123");

        write.close();
    }
}
