package print;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Writer;

public class Main {
    public static void main(String[] args) throws Exception {
        Writer out = new FileWriter("output.txt");

        PrintWriter writer = new PrintWriter(out);

        writer.printf("%5d + %5d = %5d\n", 10, 20, 30);
        writer.close();
    }
}
