package buffered;

import java.io.*;

public class Main {
    public static void main(String[] args) throws Exception {
        Reader reader = new FileReader("input.txt");
        BufferedReader bufferedReader = new BufferedReader(reader);

        System.out.println(bufferedReader.readLine());
    }

}
