package streams;

import java.io.InputStream;

public class MainInputStream2 {
    public static void main(String[] args) {
        StreamUtil streamUtil = new StreamUtil();
        InputStream input =  streamUtil.fileInputStream("input.txt");

        System.out.println(streamUtil.readAsString(input));
    }
}
