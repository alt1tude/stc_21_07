package streams;

import java.io.IOException;
import java.io.OutputStream;

public class MainOutputStream {
    public static void main(String[] args) {
        StreamUtil streamUtil = new StreamUtil();
        OutputStream output = streamUtil.fileOutputStream("output.txt");

        byte[] bytes = {65, 66, 67, 68, 69};

        try {
            output.write(bytes);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
