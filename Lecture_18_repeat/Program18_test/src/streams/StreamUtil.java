package streams;

import java.io.*;

public class StreamUtil {
    public InputStream fileInputStream(String fileName) {
        try {
            return new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

//    public int[] readAllBytes(InputStream input) {
//        int[] bytes;
//        try {
//            bytes = new int[input.available()];
//            int currentByte = input.read();
//            int i = 0;
//            while (currentByte != -1) {
//                bytes[i] = currentByte;
//                currentByte = input.read();
//                ++i;
//            }
//        } catch (IOException e) {
//            throw new IllegalStateException(e);
//        }
//
//        return bytes;
//    }

    public byte[] readAllBytes(InputStream input) {
        byte[] bytes;
        try {
            bytes = new byte[input.available()];
            int currentByte = input.read();
            int i = 0;
            while (currentByte != -1) {
                bytes[i] = (byte) currentByte;
                currentByte = input.read();
                ++i;
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return bytes;
    }

    public String readAsString(InputStream input) {
        try {
            byte[] bytes = new byte[input.available()];
            int size = input.read(bytes);
            return new String(bytes);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public OutputStream fileOutputStream (String fileName) {
        try {
            return new FileOutputStream(fileName);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
