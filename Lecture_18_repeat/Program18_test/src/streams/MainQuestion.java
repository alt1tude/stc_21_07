package streams;

public class MainQuestion {

    public static void main(String[] args) {
        // байт - от 0 до 255
        int byte1 = 255;
        // в java байт знаковый - от -128 до 127
        System.out.println((byte)byte1); // -1
    }
}
